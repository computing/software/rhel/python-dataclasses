%global pypi_name dataclasses

Name:           python-%{pypi_name}
Version:        0.8
Release:        4%{?dist}
Summary:        An implementation of PEP 557: Data Classes

License:        ASL 2.0
URL:            https://github.com/ericvsmith/dataclasses
Source0:        https://github.com/ericvsmith/%{pypi_name}/archive/%{version}.tar.gz#/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel >= 3.6
BuildRequires:  python3-devel < 3.7
BuildRequires:  python3-setuptools

%global common_description %{expand:
This package is an implementation of PEP 557, Data Classes. It is a backport
for Python 3.6, as dataclasses is included in Python 3.7 and later.}

%description
%common_description

%package -n     python%{python3_pkgversion}-%{pypi_name}
Summary:        %{summary}

%description -n python%{python3_pkgversion}-%{pypi_name}
%common_description

%prep
%autosetup -n %{pypi_name}-%{version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%check
%{python3} -m unittest discover test

%files -n python%{python3_pkgversion}-%{pypi_name}
%license LICENSE.txt
%doc README.rst
%{python3_sitelib}/__pycache__/*
%{python3_sitelib}/%{pypi_name}.py
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Thu Jan 21 2021 Patrick Godwin <patrick.godwin@ligo.org> 0.8-4
- Updates + fixes for CentOS 7 build

* Mon Jan  4 2021 Davide Cavalca <dcavalca@fedoraproject.org> - 0.8-3
- Add LICENSE.txt to files

* Mon Jan  4 2021 Davide Cavalca <dcavalca@fedoraproject.org> - 0.8-2
- Correct license
- Scope BuildRequires to Python 3.6
- Rework description

* Sun Jan  3 2021 Davide Cavalca <dcavalca@fedoraproject.org> - 0.8-1
- Initial package.
