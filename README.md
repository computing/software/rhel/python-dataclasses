# python-dataclasses

This repo contains the RPM spec file to enable creating binary RPMs for `python-dataclasses`.

## Updating this repo to the latest template

```
python -m cookiecutter https://git.ligo.org/packaging/rhel/spec-repo-template --overwrite-if-exists --output-dir .. --no-input
```
